# Verwendete Resourcen und wichtige Quellen

## Stylesheets

Für das Blog werden CSS für Bootstrap von 
http://bootswatch.com/ genutzt. Per 3.2.16 wird das CSS des Layouts
[Cosmo](http://bootswatch.com/cosmo/) genutzt.

## Blogsoftware

Für das Blog wird [JBake](http://jbake.org/) genutzt.
